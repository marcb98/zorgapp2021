import java.util.Scanner;

public class ZorgApp
{
    final static int RETURN = 0;
    final static int PATIENT = 1;
    final static int CAREGIVER = 2;

    public static void main(String[] args)
    {
        Zorgverlener zorgverlener = new Zorgverlener();

        Scanner sc = new Scanner(System.in);

        int choice = 1;
        while(choice !=0)
        {
            System.out.println("Welkom bij de ZorgApp!");
            System.out.println("0: Om te stoppen");
            System.out.println("1: Als patient inloggen");
            System.out.println("2: Als zorgverlener inloggen");

            choice = sc.nextInt();

            switch (choice) {
                case RETURN:
                    //if 0 - exit
                    break;
                case PATIENT:
                    zorgverlener.patientMenu();
                    break;
                case CAREGIVER:
                    zorgverlener.menu();
                    break;
            }
        }
    }
}
