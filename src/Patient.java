import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Scanner;

public class Patient implements Comparable<Patient>
{
   //Vars
   private String surName;
   private String firstName;
   private String callName;
   private String address;
   private LocalDate dateOfBirth;
   private double weight;
   private double length;

   private ArrayList<GraphData> graphData = new ArrayList<>();
   private ArrayList<Medicine> patientMedicines = new ArrayList<>();

   //Constructor
   Patient( String surName, String firstName, String callName, String address, LocalDate dateOfBirth, double weight, double length)
   {
      this.surName = surName;
      this.firstName = firstName;
      this.callName = callName;
      this.address = address;
      this.dateOfBirth = dateOfBirth;
      this.weight = weight;
      this.length = length;

      //test data for graphData
      graphData.add(new GraphData(LocalDate.of(2021,2,15), 65.2));
      graphData.add(new GraphData(LocalDate.of(2021,3,15), 65.5));
      graphData.add(new GraphData(LocalDate.of(2021,4,15), 65.8));
      graphData.add(new GraphData(LocalDate.of(2021,5,15), 66.1));
      graphData.add(new GraphData(LocalDate.of(2021,6,15), 66.5));
      graphData.add(new GraphData(LocalDate.of(2021,7,15), 66.4));
      graphData.add(new GraphData(LocalDate.of(2021,8,15), 66.7));
      graphData.add(new GraphData(LocalDate.of(2021,9,15), 66.9));
      graphData.add(new GraphData(LocalDate.of(2021,10,15), 70.1));
   }

   //getters and setters for attributes
   //getSurname
   public String getSurName() {
      return surName;
   }

   //setSurname
   public void setSurName(String surName) {
      this.surName = surName;
   }

   //getFirstname
   public String getFirstName() {
      return firstName;
   }

   //setFirstname
   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   //getCallname
   public String getCallName() {
      return callName;
   }

   //setCallname
   public void setCallName(String callName) {
      this.callName = callName;
   }

   //getAddress
   public String getAddress() {
      return address;
   }

   //setAddress
   public void setAddress(String address) {
      this.address = address;
   }

   //getDOB
   public LocalDate getDateOfBirth() {
      return dateOfBirth;
   }

   //setDOB
   public void setDateOfBirth(LocalDate dateOfBirth) {
      this.dateOfBirth = dateOfBirth;
   }

   //getWeight
   public double getWeight() {
      return weight;
   }

   //setWeight
   public void setWeight(double weight) {
      this.weight = weight;
   }

   //getLength
   public double getLength() {
      return length;
   }

   //setLength
   public void setLength(double length) {
      this.length = length;
   }

   public ArrayList<GraphData> getGraphData() {
      return graphData;
   }

   public void setGraphData(ArrayList<GraphData> graphData) {
      this.graphData = graphData;
   }

   public void addMedicine(Medicine m)
   {
      patientMedicines.add(m);
   }

   //compare function
   @Override
   public int compareTo(Patient e) {
      return this.getSurName().compareTo(e.getSurName());
   }

   //calculate BMI
   private double calcBmi(){
      double bmi = getWeight() / (getLength() * getLength());
      return Math.round(bmi * 100.0) / 100.0;
   }

   //calculate age
   private int calcAge() {
      LocalDate currentDate = LocalDate.now();
      return Period.between(getDateOfBirth(), currentDate).getYears();
   }

   // Write data to screen.
   void write()
   {
      System.out.println( "Achternaam:        " + getSurName() + "           - Voornaam: " + getFirstName() + "                 - Bijnaam: " + getCallName());
      System.out.println( "Adres:        " + getAddress());
      System.out.println( "Geboortedatum:  " + getDateOfBirth() + "       - Leeftijd: " + calcAge());
      System.out.println( "Gewicht:         " + getWeight() + "            - Lengte: " + getLength() + "                    - Bmi: " + calcBmi() + " - " + BMIsmiley());
      for (Medicine m : patientMedicines)
      {
         System.out.println(m.getMedicineName());
      }
   }

   //display a smiley depending on BMI range
   public String BMIsmiley()
   {
      double BMI = calcBmi();
      String smiley = "";
      if(BMI < 19.5)
      {
         smiley = "\uD83D\uDE10"; //:|
      }
      else if (BMI > 19.5 && BMI < 25)
      {
         smiley = "\uD83D\uDE42"; //:)
      }
      else if (BMI > 25 && BMI < 30)
      {
         smiley = "\uD83D\uDE10"; //:|
      }
      else if (BMI > 30)
      {
         smiley = "\uD83D\uDE41"; //:(
      }
      return smiley;
   }

   //Edit full patient, only for admin
   void editFullPatient()
   {
      //surname firstname callname address dob weight length
      Scanner sc = new Scanner(System.in);
      Scanner textInput = new Scanner(System.in);

      int choice = 1;
      while(choice != 0)
      {
         System.out.println("Kies wat u wilt veranderen:");
         System.out.println("0: Om terug te gaan.");
         System.out.println("1: Achternaam");
         System.out.println("2: Voornaam");
         System.out.println("3: Roepnaam");
         System.out.println("4: Adres");
         System.out.println("5: Geboorte datum");
         System.out.println("6: Gewicht");
         System.out.println("7: Lengte");


         choice = sc.nextInt();
         switch (choice)
         {
            case 0:
               //return
               break;
            case 1:
               //surname
               System.out.println("Vul hier onder een nieuwe achternaam in:");
               setSurName(textInput.nextLine());
               break;
            case 2:
               //firstname
               System.out.println("Vul hier onder een nieuwe voornaam in:");
               setFirstName(textInput.nextLine());
               break;
            case 3:
               //callname
               System.out.println("Vul hier onder een nieuwe roepnaam in:");
               setCallName(textInput.nextLine());
               break;
            case 4:
               //adress
               System.out.println("Vul hier onder een nieuw adres in:");
               setAddress(textInput.nextLine());
               break;
            case 5:
               //dob
               System.out.println("Vul hier onder het jaartal in:");
               int year = textInput.nextInt();
               System.out.println("Vul hier onder de maand in:");
               int month = textInput.nextInt();
               System.out.println("Vul hier onder een dag in:");
               int day = textInput.nextInt();

               setDateOfBirth(LocalDate.of(year,month,day));
               break;
            case 6:
               //weigth
               System.out.println("Vul hier onder een gewicht in(decimaal met een comma ','):");
               setWeight(Double.parseDouble(textInput.nextLine()));
               break;
            case 7:
               //length
               System.out.println("Vul hier onder een lengte in (decimaal met een comma '.'):");
               setLength(Double.parseDouble(textInput.nextLine()));
               break;
         }
      }
      System.out.println("Vul hieronder ");
   }

   public void addGraphData()
   {
      //voeg datum + gewicht
      Scanner graphInput = new Scanner(System.in);
      System.out.println("Vul hier onder het jaartal in:");
      int newYear = graphInput.nextInt();
      System.out.println("Vul hier onder de maand in:");
      int newMonth = graphInput.nextInt();
      System.out.println("Vul hier onder een dag in:");
      int newDay = graphInput.nextInt();
      System.out.println("Vul hieronder een gewicht in(decimaal met een comma ','):");
      double weight = graphInput.nextDouble();
      setWeight(weight);

      graphData.add(new GraphData(LocalDate.of(newYear,newMonth,newDay),weight));
   }
}
