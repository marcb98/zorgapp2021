import java.util.Scanner;

public class Medicine implements Comparable<Medicine>
{
    //vars
    private String medicineName;
    private String medicineDescription;
    private String medicineDose;

    //contstructor
    Medicine(String name, String description, String dose)
    {
        this.medicineName = name;
        this.medicineDescription = description;
        this.medicineDose = dose;
    }

    //getters and setters for attributes
    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineDescription(String medicineDescription) {
        this.medicineDescription = medicineDescription;
    }

    public String getMedicineDescription() {
        return medicineDescription;
    }

    public void setMedicineDose(String medicineDose) {
        this.medicineDose = medicineDose;
    }

    public String getMedicineDose() {
        return medicineDose;
    }

    @Override
    public int compareTo(Medicine o) {
        return this.getMedicineName().compareTo(o.getMedicineName());
    }

    //write all date from medicine
    public void write()
    {
        System.out.println("Medicijnnaam:         " + getMedicineName());
        System.out.println("Medicijnomschrijving: " + getMedicineDescription());
        System.out.println("Medicijndoses:        " + getMedicineDose());
    }

    //edit medicine
    public void editMedicine()
    {
        Scanner sc = new Scanner(System.in);
        Scanner textInput = new Scanner(System.in);

        int choice = 1;
        while(choice != 0)
        {
            System.out.println("Kies wat u wilt veranderen:");
            System.out.println("0: Om terug te gaan");
            System.out.println("1: Medicijnnaam");
            System.out.println("2: Medicijnomschrijving");
            System.out.println("3: Medicijndoses");


            choice = sc.nextInt();
            switch (choice)
            {
                case 0:
                    //return
                    break;
                case 1:
                    //Medicijnnaam
                    System.out.println("Vul hier onder een nieuwe Medicijnnaam in:");
                    setMedicineName(textInput.nextLine());
                    break;
                case 2:
                    //Medicijnomschrijving
                    System.out.println("Vul hier onder een nieuwe Medicijnomschrijving in:");
                    setMedicineDescription(textInput.nextLine());
                    break;
                case 3:
                    //Medicijndoses
                    System.out.println("Vul hier onder een nieuwe Medicijndoses in:");
                    setMedicineDose(textInput.nextLine());
                    break;
            }
        }
    }
}
