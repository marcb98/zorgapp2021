import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.time.LocalDate;

public class Zorgverlener
{
   //ArrayLists
   ArrayList<Patient> patientList = new ArrayList<>();
   ArrayList<Medicine> medicineList = new ArrayList<>();
   ArrayList<Caregiver> caregiverList = new ArrayList<>();

   // Constructor
   Zorgverlener()
   {
      //make some patients
      patientList.add(new Patient( "Puffelen", "Adriaan", "Ad","Goedeweg 66", LocalDate.of(1998,9,23),69.5,1.69));
      patientList.add(new Patient( "Vogel", "Karel", "Karel","Lagestraat 23", LocalDate.of(1976,2,2),80.5,1.85));
      patientList.add(new Patient( "Bakker", "Pieter", "Piet","Buitenweg 1", LocalDate.of(1989,12,6),65.5,1.72));

      //add some medicine
      medicineList.add(new Medicine("Paracetamol","Pijnstiller en koortsverlagend middel","kind 1 tablet max 3 per dag, volwassen 2 tabletten max 8 per dag"));
      medicineList.add(new Medicine("Omeprazol","Geneesmiddel bij maagklachten","Een tablet van 10mg per 12 uur"));
      medicineList.add(new Medicine("Medicijn A","Geneesmiddel voor alles","Een tablet van 35m per dag"));
      medicineList.add(new Medicine("Wondermiddel","Geneesmiddel bij maagklachten","Een tablet van 100mg per week"));

      //make some caregivers
      caregiverList.add(new Caregiver("Jan","De Vries",LocalDate.of(1978,1,12),"Arts","Afdeling 2"));
      caregiverList.add(new Caregiver("Joris","Zwaan",LocalDate.of(1967,12,19),"Zorgverlener algemeen","Afdeling 4"));
      caregiverList.add(new Caregiver("Piet","De Spade",LocalDate.of(1984,6,2),"Fysio","Afdeling 6"));


      //Sort patient and medicine A-Z
      Collections.sort(patientList);
      Collections.sort(medicineList);
      Collections.sort(caregiverList);
   }

   // Main menu
   void menu()
   {
      System.out.println("Welkom zorgverlener");
      Scanner scanner = new Scanner( System.in );

      int choice = 1;
      while ( choice !=0 )
      {
         System.out.println( "Voer uw keuze in.");
         System.out.println( "0: Om te stoppen.");
         System.out.println( "1: Laat patientenlijst zien.");
         System.out.println( "2: Voeg een patient toe.");
         System.out.println( "3: Laat medicijnenlijst zijn.");
         System.out.println( "4: Voeg medicijn toe.");
         System.out.println( "5: Laat zorgverleners zien.");
         System.out.println( "6: Voeg zorgverlener toe.");

         choice = scanner.nextInt();

         switch (choice)
         {
            case 0: // do nothing
               break;

            case 1:
               //Show list of patients
               int i = 0;
               for (Patient p : patientList)
               {
                  i++;
                  System.out.println(i + ". Achternaam: " + p.getSurName() + " - Voornaam: " + p.getFirstName());
               }

               //new switch to select a patient from list
               Scanner scanner2 = new Scanner(System.in);

               int choice2 = 1;

               while ( choice2 !=0 )
               {
                  //set input to selected patient
                  choice2 = scanner2.nextInt();
                  switch (choice2)
                  {
                     case 0:
                        //do nothing go back
                        break;
                     default:
                        int count = 0;
                        //loop through all patients
                        for (Patient p : patientList)
                        {
                           count++;
                           //execute this code if the patient == to the input
                           if (choice2 == count)
                           {
                              System.out.println("Patient nummer: "+count);
                              Scanner scanner3 = new Scanner(System.in);
                              int choice3 = 1;
                              while(choice3 != 0)
                              {
                                 p.write(); //write info of the patient

                                 //show options
                                 System.out.println( "Voer uw keuze in.");
                                 System.out.println("0: Om terug te gaan.");
                                 System.out.println("1: Patient wijzigen.");
                                 System.out.println("2: Bekijk de gewichtsgrafiek.");
                                 System.out.println("3: Gewichts punt toevoegen.");
                                 System.out.println("4: Voeg medicijn toe aan patient");

                                 choice3 = scanner3.nextInt();
                                 switch (choice3)
                                 {
                                    case 0:
                                       //return
                                       break;
                                    case 1:
                                       //edit full patient list
                                       p.editFullPatient();
                                       break;
                                    case 2:
                                       //show weight graph
                                       System.out.println("Datum ------ Gewicht");
                                       for(GraphData gd : p.getGraphData())
                                       {
                                          System.out.println(gd.getDate() +" - " + gd.getWeight());
                                       }
                                       System.out.println("0: Om terug te gaan.");
                                       int waiting = scanner3.nextInt();
                                       break;
                                    case 3:
                                       p.addGraphData();
                                       break;
                                    case 4:
                                       /*
                                       1 get medice list
                                       2 ask for a number input that is linked to medicine
                                       3 --input number
                                       4 get the medicine with the number and send it over to patient.addMedicine(number)
                                        */
                                       System.out.println("Vul het medicijn nummer in om die toe te voegen aan de patient.");
                                       System.out.println("0: Om terug te gaan.");
                                       int index = 0;
                                       //show list of all medcine
                                       for (Medicine m : medicineList)
                                       {
                                          index++;
                                          System.out.println(index+".Medcijnnaam: " + m.getMedicineName());
                                       }

                                       Scanner scanner4 = new Scanner(System.in);
                                       int choice4 = 1;
                                       while(choice4 != 0)
                                       {
                                          switch (choice4)
                                          {
                                             case 0:
                                                //return
                                                break;
                                             default:
                                                choice4 = scanner4.nextInt();
                                                int medicineCount = 0;
                                                //loop through the medicinelist
                                                for (Medicine m : medicineList)
                                                {
                                                   medicineCount++;
                                                   //compare input to medcine#
                                                   if (choice4 == medicineCount)
                                                   {
                                                      //show medicine name
                                                      System.out.println(m.getMedicineName());
                                                      //add medicine to the patient
                                                      p.addMedicine(m);
                                                   }
                                                }
                                                choice4 = 0;
                                                break;
                                          }
                                       }
                                       break;
                                 }
                              }
                           }
                        }
                        //set choice to 0 to return the list of patients
                        choice2 = 0;
                        break;
                  }
               }
               break;
            case 2:
               //add patient
               Scanner textInput = new Scanner(System.in);
               //surname
               System.out.println("Vul hier onder een nieuwe achternaam in:");
               String newSurName = textInput.nextLine();
               //firstname
               System.out.println("Vul hier onder een nieuwe voornaam in:");
               String newFirstName = textInput.nextLine();
               //callname
               System.out.println("Vul hier onder een nieuwe roepnaam in:");
               String newCallName = textInput.nextLine();
               //address
               System.out.println("Vul hier onder een nieuw adres in:");
               String newAddress = textInput.nextLine();
               //dob
               System.out.println("Vul hier onder het jaartal in:");
               int newYear = textInput.nextInt();
               System.out.println("Vul hier onder de maand in:");
               int newMonth = textInput.nextInt();
               System.out.println("Vul hier onder een dag in:");
               int newDay = textInput.nextInt();
               //weigth
               System.out.println("Vul hier onder een gewicht in(decimaal met een comma ','):");
               double newWeight = textInput.nextDouble();
               //length
               System.out.println("Vul hier onder een lengte in (decimaal met een comma ','):");
               double newLength = textInput.nextDouble();
               //add new patient to the arraylist
               patientList.add(new Patient(newSurName,newFirstName,newCallName,newAddress,LocalDate.of(newYear,newMonth,newDay),newWeight,newLength));
               break;
            case 3:
               //medijncen
               //show list
               int index = 0;
               for (Medicine medicine : medicineList)
               {
                  index++;
                  System.out.println(index+".Medcijnnaam: " + medicine.getMedicineName());
               }

               //select medicine
               Scanner sc = new Scanner(System.in);
               int medicneChoice = 1;
               while (medicneChoice != 0)
               {
                  medicneChoice = sc.nextInt();
                  //sysouttxt
                  switch (medicneChoice)
                  {
                     case 0:
                        //return
                        break;
                     default:
                        int count = 0;
                        //loop through medicine list
                        for (Medicine m : medicineList)
                        {
                           count++;
                           //run this code if the input is the same as the medicine#
                           if (medicneChoice == count)
                           {
                              System.out.println("Medicijn: "+count);
                              Scanner scanner3 = new Scanner(System.in);
                              int choice3 = 1;
                              while(choice3 != 0)
                              {
                                 //write medince info
                                 m.write();

                                 System.out.println("0: Om terug te gaan.");
                                 System.out.println("1: Medicijn wijzigen.");
                                 choice3 = scanner3.nextInt();
                                 switch (choice3)
                                 {
                                    case 0:
                                       //return
                                       break;
                                    case 1:
                                       //edit medicine
                                       m.editMedicine();
                                       break;
                                 }
                              }
                           }
                        }
                        medicneChoice = 0;
                        break;
                  }
               }
               break;
            case 4:
               //add medicine
               Scanner textInput2 = new Scanner(System.in);
               System.out.println("Vul hieronder een naam in");
               String newName = textInput2.nextLine();
               System.out.println("Vul hieronder een omschrijving in");
               String newDesc = textInput2.nextLine();
               System.out.println("Vul hieronder een dosis in");
               String newDose = textInput2.nextLine();

               medicineList.add(new Medicine(newName, newDesc, newDose));
               break;
            case 5:
               //show list
               index = 0;
               for (Caregiver c : caregiverList)
               {
                  index++;
                  System.out.println(index+".Achternaam: " + c.getSurName() + " Voornaam: " + c.getFirstName());
               }

               //Select caregiver
               Scanner scanner5 = new Scanner(System.in);
               int caregiverChoice = 1;
               while(caregiverChoice != 0)
               {
                  caregiverChoice = scanner5.nextInt();
                  switch (caregiverChoice)
                  {
                     case 0:
                        //return
                        break;
                     default:
                        int count1 = 0;
                        for (Caregiver caregiver : caregiverList)
                        {
                           count1++;
                           if(caregiverChoice == count1)
                           {
                              Scanner scanner3 = new Scanner(System.in);
                              int choice3 = 1;
                              while(choice3 != 0)
                              {
                                 caregiver.write();

                                 System.out.println("0: Om terug te gaan.");
                                 System.out.println("1: Zorgverlener wijzigen.");
                                 choice3 = scanner3.nextInt();
                                 switch (choice3)
                                 {
                                    case 0:
                                       //return
                                       break;
                                    case 1:
                                       //edit caregiver
                                       caregiver.editCaregiver();
                                       break;
                                 }
                              }
                           }
                        }
                        caregiverChoice = 0;
                        break;
                     case 6:
                        //add caregiver
                        //fn sn dob exp dep
                        Scanner SCnewCaregiver = new Scanner(System.in);
                        System.out.println("Vul hieronder een nieuwe voornaam in:");
                        String newFirstname = SCnewCaregiver.nextLine();
                        System.out.println("Vul hieronder een nieuwe achternaam in:");
                        String newSurname = SCnewCaregiver.nextLine();
                        System.out.println("Vul hier onder het jaartal in:");
                        int newYear2 = SCnewCaregiver.nextInt();
                        System.out.println("Vul hier onder de maand in:");
                        int newMonth2 = SCnewCaregiver.nextInt();
                        System.out.println("Vul hier onder een dag in:");
                        int newDay2 = SCnewCaregiver.nextInt();
                        System.out.println("Vul hieronder een nieuwe expertise in:");
                        String newExpertise = SCnewCaregiver.nextLine();
                        System.out.println("Vul hieronder een nieuwe afdeling in:");
                        String newDepartment = SCnewCaregiver.nextLine();

                        caregiverList.add(new Caregiver(newFirstname, newSurname, LocalDate.of(newYear2, newMonth2, newDay2), newExpertise, newDepartment));
                        break;
                  }
               }
               break;
            default:
               System.out.println( "Please enter a valid digit" );
               break;
         }
      }
   }

   public void patientMenu()
   {
      System.out.println("Welkom patient");
      Scanner sc = new Scanner(System.in);

      int choice = 1;
      while (choice != 0)
      {
         System.out.println( "Voer uw keuze in.");
         System.out.println("0: Om terug te gaan.");
         System.out.println("1: Inloggen patient.");

         choice = sc.nextInt();

         switch (choice)
         {
            case 0:
               //return
               break;
            case 1:
               System.out.println("Om in te loggen voor uw achternaam in");
               //new switch to select a patient from list
               Scanner scanner2 = new Scanner(System.in);

               int choice2 = 1;

               while ( choice2 !=0 )
               {
                  String inputSurname = scanner2.nextLine();
                  switch (choice2)
                  {
                     case 0:
                        //do nothing go back
                        break;
                     default:
                        String checkSurname;
                        for (Patient p : patientList)
                        {
                           checkSurname = p.getSurName();
                           if (inputSurname.equals(checkSurname))
                           {
                              Scanner scanner3 = new Scanner(System.in);
                              int choice3 = 1;
                              while(choice3 != 0)
                              {
                                 p.write();

                                 System.out.println("0: Om terug te gaan.");
                                 System.out.println("1: Roepnaam wijzigen");
                                 System.out.println("2: Gewichtsgrafiek zien");
                                 choice3 = scanner3.nextInt();
                                 switch (choice3)
                                 {
                                    case 0:
                                       //return
                                       break;
                                    case 1:
                                       Scanner scanner4 = new Scanner(System.in);
                                       System.out.println("Vul hier onder uw nieuwe roepnaam in:");
                                       String newCallname = scanner4.nextLine();
                                       p.setCallName(newCallname);
                                       break;
                                    case 2:
                                       //show weight graph
                                       System.out.println("Datum ---- Gewicht");
                                       for(GraphData gd : p.getGraphData())
                                       {
                                          System.out.println(gd.getDate() +" - " + gd.getWeight());
                                       }
                                       System.out.println("Vul 0 in om terug te gaan");
                                       int waiting = scanner3.nextInt();
                                       break;
                                 }
                              }
                           }
                        }
                        System.out.println("Deze patient bestaat niet, probeer het nog een keer.");
                        choice2 = 0;
                        break;
                  }
               }
               break;
            default:
               System.out.println("invoer");
         }
      }
   }
}
