import java.time.LocalDate;
import java.util.Scanner;

public class Caregiver implements Comparable<Caregiver>
{
    //type veranderen naar expertise
    private String firstName;
    private String surName;
    private LocalDate dateOfBirth;
    private String expertise;
    private String department;

    Caregiver(String firstName, String surName, LocalDate dateOfBirth, String expertise, String department)
    {
        this.firstName = firstName;
        this.surName = surName;
        this.dateOfBirth = dateOfBirth;
        this.department = department;
        this.expertise = expertise;
    }

    //getters and setters for attributes
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getSurName() {
        return surName;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    //function to help with sorting the arraylist A-Z
    @Override
    public int compareTo(Caregiver c) {
        return this.getSurName().compareTo(c.getSurName());
    }

    //write info of a caregiver to screen
    public void write()
    {
        System.out.println("Achternaam: " + getSurName() + " Voornaam: " + getFirstName());
        System.out.println("Geboorte datum: " + getDateOfBirth());
        System.out.println("Afdeling: " +  getDepartment() + "Expertise: " + getExpertise());
    }

    //edit a caregiver
    public void editCaregiver()
    {
        Scanner sc = new Scanner(System.in);
        Scanner textInput = new Scanner(System.in);

        int choice = 1;
        while(choice != 0)
        {
            System.out.println("Kies wat u wilt veranderen:");
            System.out.println("0: Om terug te gaan");
            System.out.println("1: Voornaam");
            System.out.println("2: Achternaam");
            System.out.println("3: Geboortedatun");
            System.out.println("4: Expertise");
            System.out.println("5: Afdeling");


            choice = sc.nextInt();
            switch (choice)
            {
                case 0:
                    //return
                    break;
                case 1:
                    //voornaam
                    System.out.println("Vul hier onder een nieuwe voornaam in:");
                    setFirstName(textInput.nextLine());
                    break;
                case 2:
                    //achternaam
                    System.out.println("Vul hier onder een nieuwe achternaam in:");
                    setSurName(textInput.nextLine());
                    break;
                case 3:
                    //geb. daum
                    System.out.println("Vul hier onder het jaartal in:");
                    int year = textInput.nextInt();
                    System.out.println("Vul hier onder de maand in:");
                    int month = textInput.nextInt();
                    System.out.println("Vul hier onder een dag in:");
                    int day = textInput.nextInt();

                    setDateOfBirth(LocalDate.of(year,month,day));
                    break;
                case 4:
                    //expertise
                    System.out.println("Vul hieronder een nieuwe expertise in:");
                    setExpertise(textInput.nextLine());
                    break;
                case 5:
                    //afdeling
                    System.out.println("Vul hieronder een nieuw afdeling in:");
                    setDepartment(textInput.nextLine());
                    break;
            }
        }
    }
}
