import java.time.LocalDate;

public class GraphData {
    private LocalDate date;
    private double weight;

    GraphData(LocalDate newDate, double newWeight)
    {
        this.date = newDate;
        this.weight = newWeight;
    }

    //getters and setters for attributes
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
